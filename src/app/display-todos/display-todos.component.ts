import { Component, OnInit, Input } from '@angular/core';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-display-todos',
  templateUrl: './display-todos.component.html',
  styleUrls: ['./display-todos.component.scss']
})
export class DisplayTodosComponent implements OnInit {
  private todos: string[];

  constructor(private todoService: TodoService) {
    this.todos = todoService.getTodos();
  }

  ngOnInit() {
  }


  delete(todo) {
    this.todoService.deleteTodo(todo);
  }
}
