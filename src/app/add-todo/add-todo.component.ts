import { Component, OnInit } from '@angular/core';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.scss']
})
export class AddTodoComponent implements OnInit {
  todo: string;

  constructor(private todoService: TodoService) { }

  private static isBlank(str: string) {
    return (!str || /^\s*$/.test(str));
  }

  ngOnInit() {
  }

  addTodo() {
    if (!AddTodoComponent.isBlank(this.todo)) {
      this.todoService.addTodo(this.todo);
    }
  }
}
