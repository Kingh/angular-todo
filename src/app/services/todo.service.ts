import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
   private _todos: string[];

  constructor() {
    this._todos = [];
  }

  public addTodo(todo) {
    this._todos.push(todo);
  }

  public getTodos() {
    return this._todos;
  }

  public deleteTodo(todo) {
    const index = this._todos.indexOf(todo);

    if (index !== -1) {
      this._todos.splice(index, 1);
    }
  }
}
